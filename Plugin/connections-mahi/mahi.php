<?php
/**
 * An extension for the Connections Business Directory plugin.
 *
 * @package   Connections Business Directory Extension - Missouri Association of Home Inspectors
 * @category  Extension
 * @author    Steven A. Zahm
 * @license   GPL-2.0+
 * @link      https://connections-pro.com
 * @copyright 2019 Steven A. Zahm
 *
 * @wordpress-plugin
 * Plugin Name:       Connections Business Directory Extension - Missouri Association of Home Inspectors
 * Plugin URI:        https://connections-pro.com/add-on/csv-import/
 * Description:       An extension for the Connections Business Directory plugin.
 * Version:           1.0
 * Author:            Steven A. Zahm
 * Author URI:        https://connections-pro.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       connections_mahi
 * Domain Path:       /languages
 */
if ( ! class_exists( 'Connections_MAHI' ) ) {

	final class Connections_MAHI {

		const VERSION = '1.0';

		/**
		 * @var Connections_MAHI Stores the instance of this class.
		 *
		 * @access private
		 * @since  1.0
		 */
		private static $instance;

		/**
		 * @var string The absolute path this this file.
		 *
		 * @access private
		 * @since  1.0
		 */
		private $file = '';

		/**
		 * @var string The URL to the plugin's folder.
		 *
		 * @access private
		 * @since  1.0
		 */
		private $url = '';

		/**
		 * @var string The absolute path to this plugin's folder.
		 *
		 * @access private
		 * @since  1.0
		 */
		private $path = '';

		/**
		 * @var string The basename of the plugin.
		 *
		 * @access private
		 * @since 2.7
		 */
		private $basename = '';

		/**
		 * A dummy constructor to prevent the class from being loaded more than once.
		 *
		 * @access public
		 * @since  1.0
		 */
		public function __construct() { /* Do nothing here */ }

		/**
		 * The main plugin instance.
		 *
		 * @since  1.0
		 *
		 * @return self
		 */
		public static function instance() {

			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Connections_MAHI ) ) {

				self::$instance = $self = new self;

				$self->file     = __FILE__;
				$self->url      = plugin_dir_url( $self->file );
				$self->path     = plugin_dir_path( $self->file );
				$self->basename = plugin_basename( $self->file );

				self::loadDependencies();
				self::hooks();

				/**
				 * This should run on the `plugins_loaded` action hook. Since the extension loads on the
				 * `plugins_loaded` action hook, load immediately.
				 */
				cnText_Domain::register(
					'connections_mahi',
					$self->basename,
					'load'
				);
			}

			return self::$instance;
		}

		private static function loadDependencies() {
		}

		private static function hooks() {

			// Register the metabox and fields.
			add_action( 'cn_metabox', array( __CLASS__, 'registerFields' ) );

			// Register the custom fields CSV Export attributes and processing callback.
			add_filter( 'cn_csv_export_fields', array( __CLASS__, 'registerCSVFieldHeader' ) );
			add_filter( 'cn_csv_export_fields_config', array( __CLASS__, 'registerCustomFieldCSVExportConfig' ) );

			// Register the custom fields CSV Import mapping options and processing callback.
			add_filter( 'cncsv_map_import_fields', array( __CLASS__, 'registerCSVFieldHeader' ) );
			add_action( 'cncsv_import_fields', array( __CLASS__, 'registerCustomFieldImportAction' ), 10, 3 );

			// Register the content blocks for the custom metaboxes.
			add_filter( 'cn_content_blocks', array( __CLASS__, 'registerContentBlocks') );

			// Register the display callbacks for the content blocks.
			add_action( 'cn_entry_output_content-disclaimer', array( __CLASS__, 'renderDisclaimer' ), 10, 3 );
			add_action( 'cn_entry_output_content-occupancy_requirements', array( __CLASS__, 'renderOccupancyRequirements' ), 10, 3 );
			add_action( 'cn_entry_output_content-sewer_lateral_insurance_program', array( __CLASS__, 'renderSewerLateralInsuranceProgram' ), 10, 3 );
			add_action( 'cn_entry_output_content-city_services_phone_directory', array( __CLASS__, 'renderCityServicesPhoneDirectory' ), 10, 3 );
			add_action( 'cn_entry_output_content-utilities', array( __CLASS__, 'renderUtilities' ), 10, 3 );
			add_action( 'cn_entry_output_content-contact-suggest_button', array( __CLASS__, 'renderButton' ), 10, 3 );

			// Register and enqueue the styles.
			add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueueStyles' ), 999 );
		}

		/**
		 * @since 1.0
		 *
		 * @return string
		 */
		public function pluginPath() {

			return $this->path;
		}

		/**
		 * @since 1.0
		 *
		 * @return string
		 */
		public function pluginURL() {

			return $this->url;
		}

		public static function enqueueStyles() {

			$url  = cnURL::makeProtocolRelative( Connections_MAHI()->pluginURL() );
			$path = Connections_MAHI()->pluginPath();

			wp_enqueue_style(
				'cn-mahi',
				"{$url}assets/css/frontend.css",
				array( 'cn-public' ),
				Connections_MAHI::VERSION . '-' . filemtime( "{$path}assets/css/frontend.css" )
			);
		}

		public static function registerFields() {

			$atts = array(
				'title'    => esc_html__( 'Occupancy Requirements', 'connections_mahi' ),
				'id'       => 'occupancy_requirements',
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => esc_html__( 'Residential Occupancy Permit Required', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_residental_occ',
						'type'       => 'select',
						'options' => array(
							''     => esc_html__( 'Select', 'connections_mahi' ),
							'na'   => esc_html__( 'N/A', 'connections_mahi' ),
							'nd'   => esc_html__( 'No Details Available', 'connections_mahi' ),
							'yes'  => esc_html__( 'Yes', 'connections_mahi' ),
							'no'   => esc_html__( 'No', 'connections_mahi' ),
							'psod' => esc_html__( 'Please See Occupancy Details', 'connections_mahi' ),
						),
						'default'    => '',
					),
					array(
						'name'       => esc_html__( 'Residential Occupancy Permit Fee', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_residental_fee',
						'type'       => 'text',
						'size'       => 'small',
					),
					array(
						'name'       => esc_html__( 'Rental Occupancy Permit Required', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_rental_occ',
						'type'       => 'select',
						'options' => array(
							''     => esc_html__( 'Select', 'connections_mahi' ),
							'na'   => esc_html__( 'N/A', 'connections_mahi' ),
							'nd'   => esc_html__( 'No Details Available', 'connections_mahi' ),
							'yes'  => esc_html__( 'Yes', 'connections_mahi' ),
							'no'   => esc_html__( 'No', 'connections_mahi' ),
							'psod' => esc_html__( 'Please See Occupancy Details', 'connections_mahi' ),
						),
						'default'    => '',
					),
					array(
						'name'       => esc_html__( 'Rental Occupancy Permit Fee', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_rental_fee',
						'type'       => 'text',
						'size'       => 'small',
					),
					array(
						'name'       => esc_html__( 'Conditional Occupancy Permit Required', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_conditional_occ',
						'type'       => 'select',
						'options' => array(
							''     => esc_html__( 'Select', 'connections_mahi' ),
							'na'   => esc_html__( 'N/A', 'connections_mahi' ),
							'nd'   => esc_html__( 'No Details Available', 'connections_mahi' ),
							'yes'  => esc_html__( 'Yes', 'connections_mahi' ),
							'no'   => esc_html__( 'No', 'connections_mahi' ),
							'psod' => esc_html__( 'Please See Occupancy Details', 'connections_mahi' ),
						),
						'default'    => '',
					),
					array(
						'name'       => esc_html__( 'Conditional Occupancy Permit Fee', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_conditional_fee',
						'type'       => 'text',
						'size'       => 'small',
					),
					array(
						'name'       => esc_html__( 'Commercial Occupancy Permit Required', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_comm_occ',
						'type'       => 'select',
						'options' => array(
							''     => esc_html__( 'Select', 'connections_mahi' ),
							'na'   => esc_html__( 'N/A', 'connections_mahi' ),
							'nd'   => esc_html__( 'No Details Available', 'connections_mahi' ),
							'yes'  => esc_html__( 'Yes', 'connections_mahi' ),
							'no'   => esc_html__( 'No', 'connections_mahi' ),
							'psod' => esc_html__( 'Please See Occupancy Details', 'connections_mahi' ),
						),
						'default'    => '',
					),
					array(
						'name'       => esc_html__( 'Commercial Occupancy Permit Fee', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_comm_fee',
						'type'       => 'text',
						'size'       => 'small',
					),
					array(
						'name'       => esc_html__( 'Fire Inspection Required', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_fire_req',
						'type'       => 'select',
						'options' => array(
							''     => esc_html__( 'Select', 'connections_mahi' ),
							'na'   => esc_html__( 'N/A', 'connections_mahi' ),
							'nd'   => esc_html__( 'No Details Available', 'connections_mahi' ),
							'yes'  => esc_html__( 'Yes', 'connections_mahi' ),
							'no'   => esc_html__( 'No', 'connections_mahi' ),
							'psod' => esc_html__( 'Please See Occupancy Details', 'connections_mahi' ),
						),
						'default'    => '',
					),
					array(
						'name'       => esc_html__( 'Fire Inspection Fee', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_fire_fee',
						'type'       => 'text',
						'size'       => 'small',
					),
					array(
						'name'       => esc_html__( 'Vacant Building Registration Required', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_comm_vac',
						'type'       => 'select',
						'options' => array(
							''     => esc_html__( 'Select', 'connections_mahi' ),
							'na'   => esc_html__( 'N/A', 'connections_mahi' ),
							'nd'   => esc_html__( 'No Details Available', 'connections_mahi' ),
							'yes'  => esc_html__( 'Yes', 'connections_mahi' ),
							'no'   => esc_html__( 'No', 'connections_mahi' ),
							'psod' => esc_html__( 'Please See Occupancy Details', 'connections_mahi' ),
						),
						'default'    => '',
					),
					array(
						'name'       => esc_html__( 'Occupancy Inspection Details', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_occ_details',
						'type'       => 'rte',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => esc_html__( 'Sewer Lateral Insurance Program', 'connections_mahi' ),
				'id'       => 'sewer_lateral_insurance_program',
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => esc_html__( 'Reimbursement', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_sewer_reimburs',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Limit of Coverage', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_sewer_limit',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Contact Information', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_sewer_phone',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Website', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_sewer_url',
						'type'       => 'text',
						'size'       => 'large',
					),
					array(
						'name'       => esc_html__( 'What portion of lateral repair / replacement is covered by program?', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_sewer_what_covered',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Sewer Lateral Program Details', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_sewer_details',
						'type'       => 'rte',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => esc_html__( 'City Services Phone Directory', 'connections_mahi' ),
				'id'       => 'city_services_phone_directory',
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => esc_html__( 'Police Department', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_police',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Fire Department', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_fire_1',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Fire Department', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_fire_2',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Animal Control', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_animal',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Building Permits', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_b_permits',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Parks and Recreation', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_parks_rec',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Public Works', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'muni_public_works',
						'type'       => 'text',
						'size'       => 'regular',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => esc_html__( 'Utilities', 'connections_mahi' ),
				'id'       => 'utilities',
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => esc_html__( 'Electricity', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'utility_electric',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Natural Gas', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'utility_gas',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Water', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'utility_water',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Sewer', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'utility_sewer',
						'type'       => 'text',
						'size'       => 'regular',
					),
					array(
						'name'       => esc_html__( 'Garbage / Recycling', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'utility_garb_recyc',
						'type'       => 'text',
						'size'       => 'regular',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => esc_html__( 'Disclaimer', 'connections_mahi' ),
				'id'       => 'disclaimer',
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => esc_html__( 'Disclaimer', 'connections_mahi' ),
						'show_label' => FALSE,
						'id'         => 'disclaimer_text',
						'type'       => 'rte',
						'default'    => 'The information on each listing is furnished and deemed reliable to the best of St. Louis ASHI’s knowledge but should be verified prior to use. St. Louis ASHI assume no liability for typographical errors, misprints or misinformation. Please check with the individual Cities if you have any questions.  These ordinances change daily; therefore, we cannot guarantee the accuracy of this information.',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => esc_html__( 'Contact / Suggest Button', 'connections_mahi' ),
				'id'       => 'contact-suggest_button',
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => esc_html__( 'Button Text', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'suggest_button_text',
						'type'       => 'text',
						'size'       => 'regular',
						'default'    => 'Suggest Edit',
					),
					array(
						'name'       => esc_html__( 'Button URL', 'connections_mahi' ),
						'show_label' => TRUE,
						'id'         => 'suggest_button_url',
						'type'       => 'text',
						'size'       => 'large',
						'default'    => 'https://stlashi.net/feedback/',
					),
				),
			);

			cnMetaboxAPI::add( $atts );
		}

		/**
		 * Callback for the `cn_csv_export_fields` action.
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param array $fields
		 *
		 * @return array
		 */
		public static function registerCSVFieldHeader( $fields ) {

			// Occupancy Requirements
			$fields['muni_residental_occ'] = esc_html__( 'Residential Occupancy Permit Required', 'connections_mahi' );
			$fields['muni_residental_fee'] = esc_html__( 'Residential Occupancy Permit Fee', 'connections_mahi' );

			$fields['muni_rental_occ'] = esc_html__( 'Rental Occupancy Permit Required', 'connections_mahi' );
			$fields['muni_rental_fee'] = esc_html__( 'Rental Occupancy Permit Fee', 'connections_mahi' );

			$fields['muni_conditional_occ'] = esc_html__( 'Conditional Occupancy Permit Required', 'connections_mahi' );
			$fields['muni_conditional_fee'] = esc_html__( 'Conditional Occupancy Permit Fee', 'connections_mahi' );

			$fields['muni_comm_occ'] = esc_html__( 'Commercial Occupancy Permit Required', 'connections_mahi' );
			$fields['muni_comm_fee'] = esc_html__( 'Commercial Occupancy Permit Fee', 'connections_mahi' );

			$fields['muni_fire_req'] = esc_html__( 'Fire Inspection Required', 'connections_mahi' );
			$fields['muni_fire_fee'] = esc_html__( 'Fire Inspection Fee', 'connections_mahi' );

			$fields['muni_comm_vac'] = esc_html__( 'Vacant Building Registration Required', 'connections_mahi' );

			$fields['muni_occ_details'] = esc_html__( 'Occupancy Inspection Details', 'connections_mahi' );

			// Sewer Lateral Insurance Program
			$fields['muni_sewer_reimburs']     = esc_html__( 'Reimbursement', 'connections_mahi' );
			$fields['muni_sewer_limit']        = esc_html__( 'Limit of Coverage', 'connections_mahi' );
			$fields['muni_sewer_phone']        = esc_html__( 'Contact Information', 'connections_mahi' );
			$fields['muni_sewer_url']          = esc_html__( 'Website', 'connections_mahi' );
			$fields['muni_sewer_what_covered'] = esc_html__( 'What portion of lateral repair / replacement is covered by program?', 'connections_mahi' );
			$fields['muni_sewer_details']      = esc_html__( 'Sewer Lateral Program Details', 'connections_mahi' );

			// City Services Phone Directory
			$fields['muni_police']        = esc_html__( 'Police Department', 'connections_mahi' );
			$fields['muni_fire_1']        = esc_html__( 'Fire Department 1', 'connections_mahi' );
			$fields['muni_fire_2']        = esc_html__( 'Fire Department 2', 'connections_mahi' );
			$fields['muni_animal']        = esc_html__( 'Animal Control', 'connections_mahi' );
			$fields['muni_b_permits']    = esc_html__( 'Building Permits', 'connections_mahi' );
			$fields['muni_parks_rec']    = esc_html__( 'Parks and Recreation', 'connections_mahi' );
			$fields['muni_public_works'] = esc_html__( 'Public Works', 'connections_mahi' );

			// Utilities
			$fields['utility_electric']   = esc_html__( 'Electricity', 'connections_mahi' );
			$fields['utility_gas']        = esc_html__( 'Natural Gas', 'connections_mahi' );
			$fields['utility_water']      = esc_html__( 'Water', 'connections_mahi' );
			$fields['utility_sewer']      = esc_html__( 'Sewer', 'connections_mahi' );
			$fields['utility_garb_recyc'] = esc_html__( 'Garbage / Recycling', 'connections_mahi' );

			// Disclaimer
			$fields['disclaimer_text'] = esc_html__( 'Disclaimer', 'connections_mahi' );

			// Contact / Suggest Button
			$fields['suggest_button_text'] = esc_html__( 'Button Text', 'connections_mahi' );
			$fields['suggest_button_url']  = esc_html__( 'Button URL', 'connections_mahi' );

			return $fields;
		}

		/**
		 * Callback for the `cn_csv_export_fields_config` filter.
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param array $fields
		 *
		 * @return array
		 */
		public static function registerCustomFieldCSVExportConfig( $fields ) {

			// Use the CSV Field Headers to setup the CSV Export config.
			// Pass an empty array so the filter callback can be used.
			$headers    = self::registerCSVFieldHeader( array() );
			$headerKeys = array_keys( $headers );

			foreach ( $headerKeys as $fieldID ) {

				$fields[] = array(
					'field'  => $fieldID,
					'type'   => 5,
					'fields' => '',
					'table'  => CN_ENTRY_TABLE_META,
					'types'  => NULL,
				);
			}

			return $fields;
		}

		/**
		 * Callback for the `cncsv_import_fields` action.
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param int         $id
		 * @param array       $row
		 * @param cnCSV_Entry $entry
		 */
		public static function registerCustomFieldImportAction( $id, $row, $entry ) {

			$data = array();
			$meta = array();

			$data['muni_residental_occ']  = $entry->arrayPull( $row, 'muni_residental_occ', 'na' );
			$data['muni_residental_fee']  = $entry->arrayPull( $row, 'muni_residental_fee', '' );
			$data['muni_rental_occ']      = $entry->arrayPull( $row, 'muni_rental_occ', 'na' );
			$data['muni_rental_fee']      = $entry->arrayPull( $row, 'muni_rental_fee', '' );
			$data['muni_conditional_occ'] = $entry->arrayPull( $row, 'muni_conditional_occ', 'na' );
			$data['muni_conditional_fee'] = $entry->arrayPull( $row, 'muni_conditional_fee', '' );
			$data['muni_comm_occ']        = $entry->arrayPull( $row, 'muni_comm_occ', 'na' );
			$data['muni_comm_fee']        = $entry->arrayPull( $row, 'muni_comm_fee', '' );
			$data['muni_fire_req']        = $entry->arrayPull( $row, 'muni_fire_req', 'na' );
			$data['muni_fire_fee']        = $entry->arrayPull( $row, 'muni_fire_fee', '' );
			$data['muni_comm_vac']        = $entry->arrayPull( $row, 'muni_comm_vac', 'na' );
			$data['muni_occ_details']     = $entry->arrayPull( $row, 'muni_occ_details', '' );

			$data['muni_sewer_reimburs']     = $entry->arrayPull( $row, 'muni_sewer_reimburs', '' );
			$data['muni_sewer_limit']        = $entry->arrayPull( $row, 'muni_sewer_limit', '' );
			$data['muni_sewer_phone']        = $entry->arrayPull( $row, 'muni_sewer_phone', '' );
			$data['muni_sewer_url']          = $entry->arrayPull( $row, 'muni_sewer_url', '' );
			$data['muni_sewer_what_covered'] = $entry->arrayPull( $row, 'muni_sewer_what_covered', '' );
			$data['muni_sewer_details']      = $entry->arrayPull( $row, 'muni_sewer_details', '' );

			$data['muni_police']       = $entry->arrayPull( $row, 'muni_police', '' );
			$data['muni_fire_1']       = $entry->arrayPull( $row, 'muni_fire_1', '' );
			$data['muni_fire_2']       = $entry->arrayPull( $row, 'muni_fire_2', '' );
			$data['muni_animal']       = $entry->arrayPull( $row, 'muni_animal', '' );
			$data['muni_b_permits']    = $entry->arrayPull( $row, 'muni_b_permits', '' );
			$data['muni_parks_rec']    = $entry->arrayPull( $row, 'muni_parks_rec', '' );
			$data['muni_public_works'] = $entry->arrayPull( $row, 'muni_public_works', '' );

			$data['utility_electric']   = $entry->arrayPull( $row, 'utility_electric', '' );
			$data['utility_gas']        = $entry->arrayPull( $row, 'utility_gas', '' );
			$data['utility_water']      = $entry->arrayPull( $row, 'utility_water', '' );
			$data['utility_sewer']      = $entry->arrayPull( $row, 'utility_sewer', '' );
			$data['utility_garb_recyc'] = $entry->arrayPull( $row, 'utility_garb_recyc', '' );

			$data['disclaimer_text'] = $entry->arrayPull( $row, 'disclaimer_text', '' );

			$data['suggest_button_text'] = $entry->arrayPull( $row, 'suggest_button_text', '' );
			$data['suggest_button_url']  = $entry->arrayPull( $row, 'suggest_button_url', '' );

			foreach ( $data as $key => $value ) {

				$meta[] = array(
					'key'   => $key,
					'value' => $value,
				);
			}

			cnEntry_Action::meta( 'update', $id, $meta );
		}

		/**
		 * Callback for the `cn_content_blocks` filter.
		 *
		 * Add the custom meta as an option in the content block settings in the admin.
		 * This is required for the output to be rendered by $entry->getContentBlock().
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param array $blocks An associative array containing the registered content block settings options.
		 *
		 * @return array
		 */
		public static function registerContentBlocks( $blocks ) {

			$blocks['disclaimer']                      = esc_html__( 'Disclaimer', 'connections_mahi' );
			$blocks['occupancy_requirements']          = esc_html__( 'Occupancy Permit Requirements', 'connections_mahi' );
			$blocks['sewer_lateral_insurance_program'] = esc_html__( 'Sewer Lateral Insurance Program', 'connections_mahi' );
			$blocks['city_services_phone_directory']   = esc_html__( 'City Services Phone Directory', 'connections_mahi' );
			$blocks['utilities']                       = esc_html__( 'Utilities', 'connections_mahi' );
			$blocks['contact-suggest_button']          = esc_html__( 'Contact / Suggest Button', 'connections_mahi' );

			return $blocks;
		}

		/**
		 * Renders the Disclaimer content block.
		 *
		 * Called by the cn_output_meta_field-disclaimer action in @see cnOutput::getContentBlock().
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param cnEntry    $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderDisclaimer( $entry, $shortcode_atts, $template ) {

			$meta = $entry->getMeta(
				array(
					'key'    => 'disclaimer_text',
					'single' => TRUE,
				)
			);

			if ( 0 < strlen( $meta ) ) {

				echo '<p class="disclaimer">' . wp_kses_post( apply_filters( 'cn_output_bio', $meta ) ) . '</p>';
			}
		}

		/**
		 * Renders the Occupancy Requirements content block.
		 *
		 * Called by the cn_entry_output_content-occupancy_requirements action in @see cnOutput::getContentBlock().
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param cnEntry    $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderOccupancyRequirements( $entry, $shortcode_atts, $template ) {

			$meta    = $entry->getMeta();
			$html    = '';
			$options = array(
				''     => esc_html__( 'Select', 'connections_mahi' ),
				'na'   => esc_html__( 'N/A', 'connections_mahi' ),
				'nd'   => esc_html__( 'No Details Available', 'connections_mahi' ),
				'yes'  => esc_html__( 'Yes', 'connections_mahi' ),
				'no'   => esc_html__( 'No', 'connections_mahi' ),
				'psod' => esc_html__( 'Please See Occupancy Details', 'connections_mahi' ),
			);

			$requirement = function( $occKey, $feeKey = NULL, $label = '' ) use ( $meta, $options ) {

				$value = cnArray::get( $meta, "{$occKey}.0", '' );
				$html  = '';

				if ( 0 < strlen( $value ) ) {

					$strings = array( $options[ $value ] );

					if ( ! is_null( $feeKey ) ) {

						if ( $fee = cnArray::get( $meta, "{$feeKey}.0" ) ) {

							$strings[] = esc_html__( 'Fee', 'connections_mahi' ) . ': ' . $fee;
						}
					}

					$strings = array_map( 'esc_html', $strings );

					ob_start();

					?>
					<li>
						<span class="occupancy-requirement-label"><?php echo $label; ?></span>
						<span class="occupancy-requirement-value"><?php echo implode( ', ', $strings ); ?></span>
					</li>
					<?php

					$html = ob_get_clean();
				}

				return $html;
			};

			$html .= $requirement( 'muni_residental_occ', 'muni_residental_fee', esc_html__( 'Residential Occupancy Permit Required', 'connections_mahi' ) );
			$html .= $requirement( 'muni_rental_occ', 'muni_rental_fee', esc_html__( 'Rental Occupancy Permit Required', 'connections_mahi' ) );
			$html .= $requirement( 'muni_conditional_occ', 'muni_conditional_fee', esc_html__( 'Conditional Occupancy Permit', 'connections_mahi' ) );
			$html .= $requirement( 'muni_comm_occ', 'muni_comm_fee', esc_html__( 'Commercial Occupancy Permit Required', 'connections_mahi' ) );
			$html .= $requirement( 'muni_fire_req', 'muni_fire_fee', esc_html__( 'Fire Inspection Required', 'connections_mahi' ) );
			$html .= $requirement( 'muni_comm_vac', NULL, esc_html__( 'Vacant Building Registration Required', 'connections_mahi' ) );

			if ( 0 < strlen( $html ) ) {

				echo '<ul class="occupancy-requirements">' . $html . '</ul>';
			}

			$details = cnArray::get( $meta, 'muni_occ_details.0', '' );

			if ( 0 < strlen( $details ) ) {

				echo '<h4 class="occupancy-inspection-details-header">' . esc_html__( 'Occupancy Inspection Details', 'connections_mahi' ) . '</h4>';

				echo '<div class="occupancy-inspection-details">' . wp_kses_post( apply_filters( 'cn_output_bio', $details ) ) . '</div>';
			}
		}

		/**
		 * Renders the Sewer Lateral Insurance Program content block.
		 *
		 * Called by the cn_entry_output_content-sewer_lateral_insurance_program action in @see cnOutput::getContentBlock().
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param cnEntry    $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderSewerLateralInsuranceProgram( $entry, $shortcode_atts, $template ) {

			$html = '';
			$meta = $entry->getMeta();

			$item = function( $key, $label ) use ( $meta ) {

				$value = cnArray::get( $meta, "{$key}.0", '' );
				$html  = '';

				if ( 0 < strlen( $value ) ) {

					ob_start();

					?>
					<li>
						<span class="sewer-lateral-insurance-program-label sewer-lateral-insurance-program-label-<?php echo sanitize_html_class( $key ); ?>"><?php echo $label; ?></span>
						<span class="sewer-lateral-insurance-program-value"><?php echo make_clickable( esc_html( $value ) ); ?></span>
					</li>
					<?php

					$html = ob_get_clean();
				}

				return $html;
			};

			$html .= $item( 'muni_sewer_reimburs', esc_html__( 'Reimbursement', 'connections_mahi' ));
			$html .= $item( 'muni_sewer_limit', esc_html__( 'Limit of Coverage', 'connections_mahi' ));
			$html .= $item( 'muni_sewer_phone', esc_html__( 'Contact Information', 'connections_mahi' ));
			$html .= $item( 'muni_sewer_url', esc_html__( 'Website', 'connections_mahi' ));
			$html .= $item( 'muni_sewer_what_covered', esc_html__( 'What portion of lateral repair / replacement is covered by program?', 'connections_mahi' ));

			if ( 0 < strlen( $html ) ) {

				echo '<ul class="sewer-lateral-insurance-program">' . $html . '</ul>';
			}

			$details = cnArray::get( $meta, 'muni_sewer_details.0', '' );

			if ( 0 < strlen( $details ) ) {

				echo '<h4 class="sewer-lateral-insurance-program-details-header">' . esc_html__( 'Sewer Lateral Program Details', 'connections_mahi' ) . '</h4>';

				echo '<div class="sewer-lateral-insurance-program-details">' . wp_kses_post( apply_filters( 'cn_output_bio', $details ) ) . '</div>';
			}
		}

		/**
		 * Renders the City Services Phone Directory content block.
		 *
		 * Called by the cn_entry_output_content-city_services_phone_directory action in @see cnOutput::getContentBlock().
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param cnEntry    $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderCityServicesPhoneDirectory( $entry, $shortcode_atts, $template ) {

			$html = '';
			$meta = $entry->getMeta();

			$item = function( array $keys, $label ) use ( $meta ) {

				$values = array();
				$html   = '';

				foreach ( $keys as $key ) {

					$value = cnArray::get( $meta, "{$key}.0", '' );

					if ( 0 < strlen( $value ) ) {

						$values[] = $value;
					}
				}

				if ( ! empty( $values ) ) {

					ob_start();

					$values = array_map( 'esc_html', $values );

					?>
					<li>
						<span class="city-services-phone-directory-label"><?php echo $label; ?></span>
						<span class="city-services-phone-directory-value"><?php echo implode( ', ', $values ); ?></span>
					</li>
					<?php

					$html = ob_get_clean();
				}

				return $html;
			};

			$html .= $item( array( 'muni_police' ), esc_html__( 'Police Department', 'connections_mahi' ));
			$html .= $item( array( 'muni_fire_1', 'muni_fire_2' ), esc_html__( 'Fire Department', 'connections_mahi' ));
			$html .= $item( array( 'muni_animal' ), esc_html__( 'Animal Control', 'connections_mahi' ));
			$html .= $item( array( 'muni_b_permits' ), esc_html__( 'Building Permits', 'connections_mahi' ));
			$html .= $item( array( 'muni_parks_rec' ), esc_html__( 'Parks and Recreation', 'connections_mahi' ));
			$html .= $item( array( 'muni_public_works' ), esc_html__( 'Public Works', 'connections_mahi' ));

			if ( 0 < strlen( $html ) ) {

				echo '<ul class="city-services-phone-directory">' . $html . '</ul>';
			}
		}

		/**
		 * Renders the Utilities content block.
		 *
		 * Called by the cn_entry_output_content-utilities action in @see cnOutput::getContentBlock().
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param cnEntry    $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderUtilities( $entry, $shortcode_atts, $template ) {

			$html = '';
			$meta = $entry->getMeta();

			$item = function( $key, $label ) use ( $meta ) {

				$value = cnArray::get( $meta, "{$key}.0", '' );
				$html  = '';

				if ( 0 < strlen( $value ) ) {

					ob_start();

					?>
					<li>
						<span class="utilities-directory-label"><?php echo $label; ?></span>
						<span class="utilities-directory-value"><?php echo esc_html( $value ); ?></span>
					</li>
					<?php

					$html = ob_get_clean();
				}

				return $html;
			};

			$html .= $item( 'utility_electric', esc_html__( 'Electricity', 'connections_mahi' ));
			$html .= $item( 'utility_gas', esc_html__( 'Natural Gas', 'connections_mahi' ));
			$html .= $item( 'utility_water', esc_html__( 'Water', 'connections_mahi' ));
			$html .= $item( 'utility_sewer', esc_html__( 'Sewer', 'connections_mahi' ));
			$html .= $item( 'utility_garb_recyc', esc_html__( 'Garbage / Recycling', 'connections_mahi' ));

			if ( 0 < strlen( $html ) ) {

				echo '<ul class="utilities-phone-directory">' . $html . '</ul>';
			}
		}

		/**
		 * Renders the Contact / Suggest Button content block.
		 *
		 * Called by the cn_entry_output_content-contact-suggest_button action in @see cnOutput::getContentBlock().
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param cnEntry    $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderButton( $entry, $shortcode_atts, $template ) {

			$meta = $entry->getMeta();

			$buttonText = cnArray::get( $meta, 'suggest_button_text.0', '' );
			$buttonURL  = cnArray::get( $meta, 'suggest_button_url.0', '' );

			if ( 0 < strlen( $buttonText ) && 0 < strlen( $buttonURL ) ) {

				?>
				<a class="button" href="<?php echo esc_url( $buttonURL ); ?>"><?php echo esc_html( $buttonText ); ?></a>
				<?php
			}
		}
	}

	/**
	 * Start up the extension.
	 *
	 * @since 1.0
	 *
	 * @return Connections_MAHI|false
	 */
	function Connections_MAHI() {

		if ( class_exists('connectionsLoad') ) {

			return Connections_MAHI::instance();

		} else {

			add_action(
				'admin_notices',
				function() {
					echo '<div id="message" class="error"><p><strong>ERROR:</strong> Connections must be installed and active in order use Connections CSV Import.</p></div>';
				}
			);

			return FALSE;
		}
	}

	/**
	 * Since Connections loads at default priority 10, and this extension is dependent on Connections,
	 * we'll load with priority 11 so we know Connections will be loaded and ready first.
	 */
	add_action( 'plugins_loaded', 'Connections_MAHI', 11 );

}
